package main

import (
	"html/template"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/simonkie/newstable/pkg/feeds"
)

func main() {
	router := gin.Default()

	feedService := feeds.FeedService{}

	go feedService.GetAllFeedsPeriodically()

	// metrics endpoint
	router.GET("/metrics", gin.WrapH(promhttp.Handler()))

	// The JSON-based API
	v1 := router.Group("/v1")
	{
		v1.GET("/feeds", func(c *gin.Context) {
			c.IndentedJSON(http.StatusOK, feedService.GetCachedFeeds())
		})
	}

	// The rendered HTML frontend
	router.SetFuncMap(template.FuncMap{
		"humanizeTimestamp": feeds.HumanizeTimestamp,
	})
	router.LoadHTMLGlob("templates/*")
	router.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", gin.H{
			"feeds": feedService.GetCachedFeeds(),
		})
	})

	if router.Run(":3000") != nil {
		os.Exit(1)
	}
}
