FROM golang:1.17 AS base

ENV GO111MODULE=on
ENV CGO_ENABLED=0
WORKDIR /src
COPY go.* .
RUN --mount=type=cache,target=/go/pkg/mod \
    go mod download

FROM base AS build

RUN --mount=target=. \
    --mount=type=cache,target=/go/pkg/mod \
    --mount=type=cache,target=/root/.cache/go-build \
    GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /app/main main.go

FROM gcr.io/distroless/static:nonroot

ENV GIN_MODE=release
COPY --from=build /app/main /newstable
COPY templates templates
USER 65532:65532
ENTRYPOINT ["/newstable"]