VERSION=v0.4.0
REGISTRY=registry.gitlab.com
IMAGE_NAME=simonkie/newstable
GOLANGCI_LINT_VERSION=v1.51.2

.PHONY: run
run:
	@go run main.go

.PHONY: lint
lint:
	docker run -t --rm -v $(shell pwd):/app -v ~/.cache/golangci-lint/:/root/.cache -w /app golangci/golangci-lint:$(GOLANGCI_LINT_VERSION) golangci-lint run -v

.PHONY: docker-build
docker-build:
	@docker build -t $(REGISTRY)/$(IMAGE_NAME):$(VERSION) .

.PHONY: docker-push
docker-push:
	@docker login $(REGISTRY)
	@docker push $(REGISTRY)/$(IMAGE_NAME):$(VERSION)

.PHONY: release
release: docker-build docker-push
