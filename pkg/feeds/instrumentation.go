package feeds

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	//nolint:gochecknoglobals // I don't know how else to do this
	FetchDuration = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "newstable_feeds_fetch_duration_seconds",
		Help: "The time it took to fetch the feeds during the last run, in seconds",
	})
)
