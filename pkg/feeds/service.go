package feeds

import (
	"fmt"
	"sort"
	"time"

	"github.com/dustin/go-humanize"
	"github.com/mmcdole/gofeed"
)

const (
	fetchIntervall = 10 * time.Minute
)

type FeedService struct {
	Cache map[string]*NewstableFeed
	Feeds map[string]string
}

type NewstableFeed struct {
	Title     string     `json:"title"`
	ID        string     `json:"id"`
	Items     []FeedItem `json:"items"`
	LastFetch time.Time  `json:"lastFetch"`
}

type FeedItem struct {
	Title       string    `json:"title"`
	Description string    `json:"description"`
	Timestamp   time.Time `json:"timestamp"`
	Link        string    `json:"link"`
}

func (fs *FeedService) loadDefaultFeedURLs() {
	feeds := map[string]string{}
	feeds["tas"] = "https://www.tagesschau.de/xml/rss2/"
	feeds["nyt"] = "https://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml"
	feeds["spo"] = "https://www.spiegel.de/schlagzeilen/index.rss"
	feeds["sdz"] = "https://rss.sueddeutsche.de/rss/Topthemen"
	feeds["bbc"] = "http://feeds.bbci.co.uk/news/rss.xml"
	feeds["faz"] = "https://www.faz.net/rss/aktuell/"
	fs.Feeds = feeds
}

func sortByTimestampDesc(items []FeedItem) []FeedItem {
	sort.Slice(items, func(i, j int) bool {
		return items[i].Timestamp.After(items[j].Timestamp)
	})

	return items
}

func convertGoFeedToNewsTabelFeed(feed *gofeed.Feed) *NewstableFeed {
	if feed == nil {
		return nil
	}

	newFeed := &NewstableFeed{}
	newFeed.Items = []FeedItem{}
	newFeed.Title = feed.Title
	newFeed.LastFetch = time.Now()

	for _, item := range feed.Items {
		newItem := FeedItem{}
		newItem.Title = item.Title
		newItem.Description = item.Description
		newItem.Timestamp = *item.PublishedParsed
		newItem.Link = item.Link
		newFeed.Items = append(newFeed.Items, newItem)
	}

	newFeed.Items = sortByTimestampDesc(newFeed.Items)[:10]

	return newFeed
}

func (fs *FeedService) orderFeeds() {
	orderedFeeds := map[string]*NewstableFeed{}

	for feedName := range fs.Feeds {
		if fs.Cache[feedName] == nil {
			continue
		}

		orderedFeeds[feedName] = fs.Cache[feedName]
	}

	fs.Cache = orderedFeeds
}

func (fs *FeedService) getAllFeeds() {
	start := time.Now()

	fp := gofeed.NewParser()

	feeds := map[string]*NewstableFeed{}

	for feedName, feedURL := range fs.Feeds {
		feed, _ := fp.ParseURL(feedURL)
		newFeed := convertGoFeedToNewsTabelFeed(feed)

		if newFeed == nil {
			fmt.Println("Skipping empty feed", feedName)

			continue
		}

		newFeed.ID = feedName
		feeds[feedName] = newFeed
	}

	fs.Cache = feeds
	fs.orderFeeds()

	elapsed := time.Since(start)
	FetchDuration.Set(elapsed.Seconds())
}

func (fs *FeedService) GetAllFeedsPeriodically() {
	fs.loadDefaultFeedURLs()
	fs.getAllFeeds()

	fmt.Println("Completed initial sync, starting periodic sync...")

	for range time.Tick(fetchIntervall) {
		fs.getAllFeeds()
		fmt.Println("Called the feed syncer")
	}
}

func (fs *FeedService) GetCachedFeeds() map[string]*NewstableFeed {
	return fs.Cache
}

func (fs *FeedService) AddFeed(feedName, feedURL string) bool {
	fs.Feeds[feedName] = feedURL
	fs.getAllFeeds()

	return true
}

func HumanizeTimestamp(timestamp time.Time) string {
	return humanize.Time(timestamp)
}
